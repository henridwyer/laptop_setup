xcode-select --install
python3 -m venv ansible_venv
source ansible_venv/bin/activate
pip3 install -U pip
pip3 install ansible
ansible-galaxy install -r requirements.yml 
ansible-playbook initial_playbook.yml -i inventory -K
