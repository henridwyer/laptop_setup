PROMPT='%(?.%F{green}√.%F{red}?%?)%f %B%F{240}%3~%f%b %# '

bindkey -v
bindkey "^R" history-incremental-pattern-search-backward

alias ll='ls -al'

HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history
setopt EXTENDED_HISTORY
# adds commands as they are typed, not at shell exit
setopt INC_APPEND_HISTORY
# share history across multiple zsh sessions
setopt SHARE_HISTORY
# append to history
setopt APPEND_HISTORY
